#!/usr/bin/env perl

use warnings;
use strict;
use 5.10.0;

use URI;

my @samples = (
  'http://www.perl.com',
  'http://www.perl.com/#somebullshit',
  'file:///home/brennen/p1k3',
  'urn:vimwiki:p1k3',
  'mailto:pieces@chaff.p1k3.com',
  'pieces@chaff.p1k3.com',
);

foreach my $sample (@samples) {
  my $u = URI->new($sample);

  say "";
  say $sample . " :::::: ";
  say "scheme\t" . $u->scheme;
  # say "authority\t" . $u->authority;
  say "opaque\t" . $u->opaque;
  say "path:\t" . $u->path;
  say "fragment:\t" . $u->fragment;
  say "recognized?\t" . $u->has_recognized_scheme;
  say "canonical:\t" . $u->canonical;
}
